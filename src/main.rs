use std::env;

fn main() {
    let args: Vec<_> = env::args().collect();
    if args.len() > 1 {
        let amount = args[1].parse::<usize>().unwrap();
        let mut bounds = Vec::<(u32,u32)>::with_capacity(amount);
        let (v, ips_tuple) = args.split_at(2);
        let mut ips_tuple_u32 : u32 = ips_tuple.map(|full_addr| convert_tup(full_addr)).collect();
        for i in 2..amount+2 {
            let ip_sub_split: Vec<&str> = args[i].split("/").collect();
            let subnet = mask_to_int(ip_sub_split[1].parse::<u32>().unwrap());
            let ipint = ip_to_int(ip_sub_split[0]);
            let mut subresult = 0;
            let mut index = 0;
            for (ind, viptup) in bounds.iter().enumerate() {
                let (tupip, tupsub) : (u32,u32) =*viptup;
                 subresult = choose_subnet(tupip,tupsub, ipint, subnet);
                 println!("IP1: {} , IP2: {}, SUB1: {:b} ,SUB2: {:b} , result: {}", int_to_ip(tupip), int_to_ip(ipint), tupsub, subnet, subresult);
                if subresult != 0 { 
                    index = ind;
                    break; 
                    }
            }
            if subresult ==0 {
                bounds.push((ipint,subnet))
            } else if subresult == 2 {
                bounds.remove(index);
                bounds.push((ipint,subnet))
            }          
        }
        for tup in bounds.iter(){
            let (ip,sub)  = *tup;
            println!("{}/{}", int_to_ip(ip),int_to_mask(sub));
        }
    }
}

fn choose_subnet(ip1: u32, sub1:u32, ip2:u32, sub2:u32) -> i32 {
    let quad = (ip1,sub1,ip2,sub2);
    match quad {
        (ip1,sub1,ip2,sub2) if ((sub1 <= sub2) && (ip1 == (ip1 & sub1 & ip2) )) => 1,
        (ip1,sub1,ip2,sub2) if ((sub1 > sub2) && (ip2 == (ip1 & ip2 & sub2))) => 2,
        _ => 0
    }
}

fn convert_tup(iptup: String) -> (u32,u32){
    let tup_split: Vec<&str> = iptup.split("/").collect();
    (ip_to_int(tup_split[0]),mask_to_int(tup_split[1].parse::<u32>().unwrap()))
}

fn ip_to_int(ip: &str) -> u32 {
    let iptup: Vec<&str> = ip.split(".").collect();
    let mut ipint: u32 = 0;
    for quad in iptup.iter() {
        ipint <<= 8;
        ipint |= quad.parse::<u32>().unwrap() & 0xff;
    }
    ipint
}

fn int_to_ip(ip: u32) -> String {
    let ip1 = ip & 0xff;
    let ip2 = (ip >> 8) & 0xff;
    let ip3 = (ip >> 16) & 0xff;
    let ip4 = (ip >> 24)  & 0xff;
    format!("{}.{}.{}.{}",ip4,ip3,ip2,ip1)
}

fn mask_to_int(nbit: u32) -> u32 {
    0xffffffff << (32-nbit)
}

fn int_to_mask(nbit: u32) -> u32 {
    let mut cnt = 0;
    let mut mbit: u32 = nbit;
    loop {
       if mbit == 0 {break;} 
       mbit <<= 1;
       cnt+=1;
    }
    cnt
}